# apiAutoTestWeb
> 基本为apiAutoTest使用FastAPI + Vue的前后端的分离版本, 不具备落地效果，可做CRUD练习参考，项目不再维护

# 目前完善功能
- [x] 登录
- [x] 项目管理
- [x] 接口管理
- [x] 环境管理
- [x] 用例管理
- [x] 报告管理
- [x] 扩展脚本

# 文档资料
[apiAutoTestWeb说明文档](backend/apiAutoTestWeb使用说明.md)
[后端说明文档](backend/readme.md)
[前端说明文档](frontend/README.md)


# 部署
1. git clone https://gitee.com/zy7y/apiAutoTestWeb.git
2. 安装docker: https://www.cnblogs.com/zy7y/p/14344375.html
3. 安装docker-compose : https://blog.csdn.net/qq_36640395/article/details/107449652 需要安装Python3， 然后pip命令换成pip3
4. 修改`frontend/.env.production`文件中的`VUE_APP_BASE_API`为自己服务器的地址
5. 在apiAutoTestWeb目录下执行`构建镜像 docker-compose build 运行服务 docker-compose up -d`
6. 访问前端`IP:2152`, 访问接口文档`IP:1328`
## 参考资料
1. https://blog.csdn.net/qq_38225558/article/details/103068220
2. https://cli.vuejs.org/zh/guide/deployment.html#docker-nginx
3. https://www.cnblogs.com/xr210/p/12676811.html
# 最后

1. 感谢在此过程中我查过的所有资料的作者，提供答案的网友
2. 该项目作为一个`Demo`，任有很多`Bug`和冗余代码可以优化
   - Github: https://github.com/zy7y/apiAutoTestWeb.git