import request from "@/utils/request";
/**
 * 使用说明 api
 * @returns
 */

export function help_doc() {
  return request({
    url: "/help"
  });
}

export function get_code() {
  return request({
    url: "/code"
  })
}

export function put_code(code){
  return request({
    url: "/code",
    method: "put",
    data: {code}
  })
}

// 前端调试接口功能
export function sendApi(data){
  if (data.content_type == "PARAMS"){
    return request({
      url: data.path,
      method: data.method,
      params: data.params,
      headers: data.header
   })
  }
  return request({
    url: data.path,
    method: data.method,
    data: data.data,
    headers: data.header
 })
}