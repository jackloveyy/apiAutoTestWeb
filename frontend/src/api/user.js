import request from "@/utils/request";


// 本地接口的 swagger接口文档登录支持
export function login(data) {
  data = `username=${data.username}&password=${data.password}`
  return request({
    url: "/login",
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    method: "post",
    data
  });
}

// 不支持swagger json请求登录接口
// export function login(data) {
//   return request({
//     url: "/login",
//     method: "post",
//     data
//   });
// }


export function getInfo() {
  return request({
    url: "/info",
    method: "get"
  });
}

export function logout() {
  return request({
    url: "/logout",
    method: "post"
  });
}

export function list(){
  return request({
    url: "/list"
  })
}